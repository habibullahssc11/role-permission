@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">

                <div class="card-header">
                    Dashboard
                    @role('writer|admin')
                    <a class="float-right" href="{{ route('post.create') }}">Add new</a>
                    @endrole

                </div>



                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    <table class="table">
                        <thead>
                            <tr>
                                <td>#SL</td>
                                <td>Title</td>
                                <td>Action</td>
                            </tr>
                        </thead>

                        <tbody>
                            @php( $i = 1 )
                                @foreach( $posts as $post )
                                <tr>
                                    <td>{{ $i  }}</td>
                                    <td> {{ $post['title'] }}</td>
                                    <td>
                                        @can('edit post')
                                        <a href="{{ route('post.edit', $post['id']) }}">Edit</a>
                                        @endcan
                                    </td>
                                </tr>
                                @php( $i++ )
                            @endforeach()
                        </tbody>

                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
