@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">

                <div class="card-header">
                    Post create
                    @role('writer|admin')
                    <a class="float-right" href="{{ route('post.create') }}">Add new</a>
                    @endrole

                </div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                        <form action="post" action="{{ route('post.store') }}">
                            <div class="form-group">
                                <label>Title</label>
                                <input type="text" name="title" class="form-control"/>
                            </div>

                            <div class="form-group">
                                <label>Content</label>
                                <textarea name="body" class="form-control" rows="6"></textarea>
                            </div>
                        </form>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
