<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/post', 'PostsController@index')->name('post.index');
Route::get('/post/create', 'PostsController@create')->name('post.create')->middleware('role:writer|admin');
Route::post('/post', 'PostsController@store')->name('post.store');
Route::get('/post/{post}', 'PostsController@show')->name('post.show');
Route::put('/post/{post}', 'PostsController@update')->name('post.update');
Route::delete('/post/{post}', 'PostsController@destroy')->name('post.destroy');
Route::get('/post/{post}/edit', 'PostsController@edit')->name('post.edit')->middleware('permission:edit post');
