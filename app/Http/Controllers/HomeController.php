<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use App\User;
class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        //Role::create(['name'=>'writer']);
        //$permission = Permission::create(['name'=>'Edit post']);
        //$role = Role::findById(1);
        //$permission = Permission::findById(2);
        //$role->givePermissionTo($permission);
        //$permission->removeRole($role);
        //$role->revokePermissionTo($permission);

        // Adding permissions to a user
        //auth()->user()->givePermissionTo('edit post');

        // Adding permissions via a role
        //auth()->user()->assignRole('writer');
        //return $permissions = auth()->user()->permissions;
        //return $permissions = auth()->user()->getDirectPermissions();
        //return $permissions = auth()->user()->getPermissionsViaRoles();
        //return $permissions = auth()->user()->getAllPermissions();
        // get user list
        //return User::role('writer')->get();
        //return User::permission('writer post')->get();
        //return auth()->user()->revokePermissionTo('edit post');
        //return auth()->user()->removeRole('writer');
        return view('home');
    }
}
